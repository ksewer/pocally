package sample;
import javafx.application.Platform;
import javafx.scene.control.Label;
import sample.Controllers.ControllerMemory;


public class Timer implements Runnable{

    int total_time;
    int totalTimeToChange;
    int sec;
    int min;
    String minString;
    String secString;
    public static String timeToShowLabel="00:00";

        Label timeLabel;

        public Timer(Label IdTime) {
            timeLabel = IdTime;
        }

        @Override
        public void run() {


            while (ControllerMemory.memoryRun==true) {
                try {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            total_time++;
                            totalTimeToChange=total_time;

                            min=0;
                            sec=0;
                            while(totalTimeToChange>59){
                                min++;
                                totalTimeToChange=totalTimeToChange-60;
                            }
                            sec=totalTimeToChange;

                            if(min<10){
                                minString="0"+Integer.toString(min);
                            }else{
                                minString=Integer.toString(min);
                            }
                            if(sec<10){
                                secString="0"+Integer.toString(sec);
                            }else{
                                secString=Integer.toString(sec);
                            }
                            setTimeLabel();
                        }
                    });
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    e.printStackTrace();
                }
            }
        }

    public void setTimeLabel(){
        timeToShowLabel=minString+" min  "+secString+" sec";
        timeLabel.setText(minString+":"+secString);
    }

}
