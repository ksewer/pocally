package sample;

import sample.Controllers.ControllerMenu;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class Wisielec {
    private JPanel mainPanel;
    private JPanel PolePrzyciskow;
    private JButton buttonBack;
    private JButton buttonReset;
    private JTextArea hasloLabel;
    private JLabel wybranyCzasLabel;
    private JLabel wisielecImg;
    private JLabel totalScoreLabel;
    private int buttonSize=30;
    private String hasloNapis="X";
    private char[] hasloTablica;
    private char[] hasloDoWyswietlenia;
    private String hasloDoWyswietleniaString="";
    private int dlugoscHasla;
    private int iloscPomylek;
    private String sciezkaDoImg;
    private  JButton[] tablicaPrzyciskow;
    public JFrame frameWisielecGra=new JFrame();
    public String czas="Present Simple";
    public String[] haslaPobrane;
    Random generator = new Random();
    private int wylosowaneHaslo;


    public void przypiszDaneStartoweWisielec(){
        Container ButtonsPanel;
        ButtonsPanel = PolePrzyciskow;
        ButtonsPanel.setLayout(new GridLayout(4,7));
        ButtonsPanel.setMaximumSize(new Dimension(500,400));
        tablicaPrzyciskow=new JButton[26];
        hasloLabel.setMaximumSize(new Dimension(500,200));
        hasloLabel.setMinimumSize(new Dimension(500,100));
        hasloLabel.setPreferredSize(new Dimension(500,100));
        hasloLabel.setLineWrap(true);
        hasloLabel.setWrapStyleWord(true);
        hasloLabel.setEditable(false);
        wybranyCzasLabel.setText(ControllerMenu.category);
        hasloLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        hasloLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        totalScoreLabel.setText("Your total score: "+Integer.toString(ControllerMenu.totalScore));


        for (int i=0; i<26; i++){
            final JButton button = new JButton();

            //65 A
            button.setName(Integer.toString(i));
            button.setMaximumSize(new Dimension(buttonSize, buttonSize));
            button.setMaximumSize(new Dimension(buttonSize, buttonSize));
            button.setFocusPainted(false);
            button.setFont(new Font("Impact", Font.PLAIN, 30));

            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    sprawdzPrzycisk(button);
                }
            });
            PolePrzyciskow.add(button);
            tablicaPrzyciskow[i]=button;

        }
        tablicaPrzyciskow[0].setText("A");
        tablicaPrzyciskow[1].setText("B");
        tablicaPrzyciskow[2].setText("C");
        tablicaPrzyciskow[3].setText("D");
        tablicaPrzyciskow[4].setText("E");
        tablicaPrzyciskow[5].setText("F");
        tablicaPrzyciskow[6].setText("G");
        tablicaPrzyciskow[7].setText("H");
        tablicaPrzyciskow[8].setText("I");
        tablicaPrzyciskow[9].setText("J");
        tablicaPrzyciskow[10].setText("K");
        tablicaPrzyciskow[11].setText("L");
        tablicaPrzyciskow[12].setText("M");
        tablicaPrzyciskow[13].setText("N");
        tablicaPrzyciskow[14].setText("O");
        tablicaPrzyciskow[15].setText("P");
        tablicaPrzyciskow[16].setText("Q");
        tablicaPrzyciskow[17].setText("R");
        tablicaPrzyciskow[18].setText("S");
        tablicaPrzyciskow[19].setText("T");
        tablicaPrzyciskow[20].setText("U");
        tablicaPrzyciskow[21].setText("V");
        tablicaPrzyciskow[22].setText("W");
        tablicaPrzyciskow[23].setText("X");
        tablicaPrzyciskow[24].setText("Y");
        tablicaPrzyciskow[25].setText("Z");

        generowanieHasla();
        buttonBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frameWisielecGra.dispose();
                ControllerMenu.wisielecOpen=false;
            }
        });
//        buttonReset.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
////                Start.frameWisielecGra.dispose();
////                Start.PokazWisielecWybor();
//            }
//        });
    }

    public void pokazWisielec(Wisielec gra){
        frameWisielecGra.setContentPane(gra.mainPanel);
        frameWisielecGra.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frameWisielecGra.pack();
        frameWisielecGra.setVisible(true);
        frameWisielecGra.setLocationRelativeTo(null);
        frameWisielecGra.setExtendedState(JFrame.MAXIMIZED_BOTH);
    }

    private void generowanieHasla(){
        wybranyCzasLabel.setText(ControllerMenu.category);
        haslaPobrane=RWFile.ReadFile("src/db/Hangman/"+ControllerMenu.category+".txt");
        wylosowaneHaslo=generator.nextInt(haslaPobrane.length);
        hasloNapis=haslaPobrane[wylosowaneHaslo].toUpperCase();
        System.out.println(hasloNapis);
        dlugoscHasla=hasloNapis.length();
        if (dlugoscHasla<50) hasloLabel.setFont(new Font("Impact", Font.PLAIN, 48));
        else if (dlugoscHasla<100) hasloLabel.setFont(new Font("Impact", Font.PLAIN, 36));
        else hasloLabel.setFont(new Font("Impact", Font.PLAIN, 26));
        //System.out.println(hasloNapis);
        hasloTablica = new char[dlugoscHasla];
        hasloDoWyswietlenia = new char[dlugoscHasla];
        hasloTablica=hasloNapis.toCharArray();
        hasloDoWyswietleniaString="";
        for(int i=0; i<dlugoscHasla; i++){
            if(hasloTablica[i]==' '){
                hasloDoWyswietlenia[i]=' ';
            }
            else if ((hasloTablica[i]>64 && hasloTablica[i]<91) || (hasloTablica[i]==260 || hasloTablica[i]==262 || hasloTablica[i]==280 || hasloTablica[i]==321 || hasloTablica[i]==323 || hasloTablica[i]==211 || hasloTablica[i]==346 || hasloTablica[i]==377 || hasloTablica[i]==379)){
                hasloDoWyswietlenia[i]='_';
            }
            else {
                hasloDoWyswietlenia[i]=hasloTablica[i];
            }
        }
        obslugaWyswietlaniaHasla();

    }

    private void obslugaWyswietlaniaHasla(){
        for(int i=0; i<dlugoscHasla; i++){
            if(hasloDoWyswietlenia[i]=='_'){
                hasloDoWyswietleniaString=hasloDoWyswietleniaString+hasloDoWyswietlenia[i]+" ";
            }
            else if(hasloDoWyswietlenia[i]==' ')
                hasloDoWyswietleniaString=hasloDoWyswietleniaString+hasloDoWyswietlenia[i]+"     ";
            else hasloDoWyswietleniaString=hasloDoWyswietleniaString+hasloDoWyswietlenia[i];
        }
        hasloLabel.setText(hasloDoWyswietleniaString);
    }

    private void sprawdzPrzycisk(JButton przycisk){
        char literaSprawdzana[]=przycisk.getText().toCharArray();
        char literaSprawdzana1=literaSprawdzana[0];
        //System.out.println(literaSprawdzana1);
        boolean czyTrafil=false;
        hasloDoWyswietleniaString="";
        for(int i=0; i<dlugoscHasla; i++){
            if (hasloTablica[i]==literaSprawdzana1){
                hasloDoWyswietlenia[i] = literaSprawdzana1;
                czyTrafil=true;
            }
        }
        obslugaWyswietlaniaHasla();
        if(czyTrafil){
            przycisk.setBackground(Color.GREEN);
            przycisk.setEnabled(false);
        }
        else {
            iloscPomylek++;
            sciezkaDoImg="/wisielec/";
            sciezkaDoImg=sciezkaDoImg+Integer.toString(iloscPomylek)+".png";
            wisielecImg.setIcon(new ImageIcon(Class.class.getResource(sciezkaDoImg)));
            przycisk.setBackground(Color.RED);
            przycisk.setEnabled(false);
        }
        sprawdzWygrana();
    }

    private void sprawdzWygrana(){
        if(iloscPomylek>7) {
            JOptionPane.showConfirmDialog(null, "Try again...", "Defeat :-(", JOptionPane.CLOSED_OPTION);
            for (int i = 0; i < 26; i++) {
                tablicaPrzyciskow[i].setEnabled(false);
            }
            hasloLabel.setText(hasloNapis);
            hasloLabel.setForeground(Color.RED);
        }
        boolean czyZgodneHaslo=true;
        for(int i=0; i<dlugoscHasla; i++){
            if(hasloTablica[i]!=hasloDoWyswietlenia[i])czyZgodneHaslo=false;
        }
        if(czyZgodneHaslo){
            for (int i = 0; i < 26; i++) {
                tablicaPrzyciskow[i].setEnabled(false);
            }
            hasloLabel.setForeground(Color.RED);
            wisielecImg.setIcon(new ImageIcon(Class.class.getResource("/wisielec/wygrana.png")));
            JOptionPane.showConfirmDialog(null, "GOOD JOB!!! :-)", "Nice!!!", JOptionPane.CLOSED_OPTION);
            ControllerMenu.totalScore=ControllerMenu.totalScore+2;
            totalScoreLabel.setText("Your total score: "+Integer.toString(ControllerMenu.totalScore));
        }
    }

}
