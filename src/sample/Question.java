package sample;

public class Question {
    private String pytanie;
    private String odpowiedz;
    private String[] tab_odp=new String[4];
    private int poprawna_odp=-1;


    public String getPytanie() {
        return pytanie;
    }

    public void setPytanie(String question) {
        this.pytanie = question;
    }

    public String getOdpowiedz() {
        return odpowiedz;
    }

    public void setOdpowiedz(String answer) {
        this.odpowiedz = answer;
        przypiszOdp(answer);
    }

    private void przypiszOdp(String odpowiedz){
        tab_odp=odpowiedz.split(",");
        poprawna_odp=Integer.parseInt(tab_odp[3]);
    }

    public int getPoprawna_odp() {
        return poprawna_odp;
    }

    public String[] getOdp(){
        return tab_odp;
    }

}
