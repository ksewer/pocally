package sample.Controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.Database.DBConnection;
import sample.Database.PasswordHash;
import sample.Database.UsersDB;
import sample.User;
import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class LoginController {
    //private static Stage MainStage;
    public static User[] users;
    private String[] usersDB;
    public static int user_login;
    private String[] usersString;
    private int iloscUzytkownikow;
    public static ControllerMenu przechwycony;
    DBConnection dbConnection;


    public LoginController(){
        dbConnection = new DBConnection();
        pobierzUzytkownikow();
    }


    private void pobierzUzytkownikow(){
/////////////////////////////////////////////////////////////////////////////
        try (
                Connection con = dbConnection.getConnection();
                Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                ResultSet rs = stmt.executeQuery("SELECT * FROM `users`");

        ) {

            //rs.last();

            System.out.println("Connected");
            System.out.println(rs.getRow());
            users=UsersDB.getUsers(rs);


        } catch (SQLException e) {
            System.err.print(e);
        }

        iloscUzytkownikow=users.length;
//////////////////////////////////////////////////////////////////////////////
//        usersString= RWFile.ReadFile("src/db/users.txt");
//        for(int i=0; i<usersString.length; i++){
//            usersString[i]=usersString[i].trim();
//            if(usersString[i].equals("next")){
//                iloscUzytkownikow++;
//            }
//        }

//        users=new User[iloscUzytkownikow];
//        for(int i=0; i<iloscUzytkownikow; i++){
//            users[i]=new User();
//        }
//        int j=0;
//        for(int i=0;i<usersString.length;i++){
//            if(usersString[i].equals("next"))continue;
//            else{
//                users[j].setLogin(usersString[i++]);
//                users[j].setPassword(usersString[i++]);
//                users[j].score=Integer.parseInt(usersString[i++]);
//                users[j].setEmail(usersString[i]);
//                j++;
//            }
//        }
    }
    @FXML TextField tf_username;
    @FXML public TextField tf_password;
    SignUpController new_account;

    public void loginFunction(){
        boolean correctLogin=false;
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("Wrong username or password!");
        alert.setContentText("Type correct username and password or Sign up!");
        String username=tf_username.getText();
        String password= null;
        try {
            password = PasswordHash.hashPassword(tf_password.getText());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        for(int i=0; i<iloscUzytkownikow; i++){
            if(users[i].getLogin().equals(username)){
                if(users[i].getPassword().equals(password)){
                    System.out.println("logowanie udane");
                    correctLogin=true;
                    pokazMenu();
                    user_login=i;
                    przechwycony.setUserInfo(users[i]);
                }
            }
        }
        if(!correctLogin){
            alert.showAndWait();
        }
    }

    public void pokazMenu(){
        try {
            URL url = getClass().getClassLoader().getResource("sample/Controllers/FXMLTemplates/Menu.fxml");
            FXMLLoader loader = new FXMLLoader(url);
            Stage stage = (Stage) tf_password.getScene().getWindow();

            stage.setResizable(true);
            Scene scene = new Scene(loader.load());
            stage.setScene(scene);
            przechwycony=loader.getController();
            //setWindowMenuSize(600,600);

        }catch (IOException io){
            io.printStackTrace();
        }
    }

    public void showSignUp(){
        try {
            URL url = getClass().getClassLoader().getResource("sample/Controllers/FXMLTemplates/SignUp.fxml");
            FXMLLoader loader = new FXMLLoader(url);
            Stage stage = (Stage) tf_password.getScene().getWindow();

            stage.setResizable(true);
            Scene scene = new Scene(loader.load());
            stage.setScene(scene);
            new_account=loader.getController();
            new_account.setUsers(users);
            //setWindowMenuSize(600,600);

        }catch (IOException io){
            io.printStackTrace();
        }
    }
}
