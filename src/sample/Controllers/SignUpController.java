package sample.Controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.Database.DBConnection;
import sample.Database.PasswordHash;
import sample.Database.UsersDB;
import sample.RWFile;
import sample.User;

import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.sql.*;

public class SignUpController {
    @FXML
    TextField su_username;
    @FXML
    TextField su_email;
    @FXML
    PasswordField su_password;
    @FXML
    PasswordField su_password1;

    User users[];
    User users_with_new_user[];
    String new_password;

    public void showLogin(){

        try {
            URL url = getClass().getClassLoader().getResource("sample/Controllers/FXMLTemplates/Login.fxml");
            FXMLLoader loader = new FXMLLoader(url);
            Stage stage = (Stage) su_password.getScene().getWindow();

            stage.setResizable(true);
            Scene scene = new Scene(loader.load());
            stage.setScene(scene);
            //przechwycony=loader.getController();
            //setWindowMenuSize(600,600);

        }catch (IOException io){
            io.printStackTrace();
        }
    }

    public void setUsers(User[] users){
       this.users=users;
       users_with_new_user=new User[users.length+1];
    }

    public void addAccount() {
        boolean goodData=true;
        if (su_password.getText().equals("") || su_email.getText().equals("") || su_email.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Complete all text field!");
//            alert.setContentText("Type correct username and password or Sign up!");
            alert.showAndWait();
        } else {

            for(int i =0;i<users.length;i++ ){
                users_with_new_user[i]=users[i];
                if(su_username.getText().equals(users[i].getLogin())){
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText("The username is already in use!");
                    alert.setContentText("Enter different username and try again");
                    goodData=false;
                    alert.showAndWait();
                }
                if(su_email.getText().equals(users[i].getEmail())){
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText("The email is already in use!");
                    alert.setContentText("Enter different email and try again");
                    goodData=false;
                    alert.showAndWait();
                }
            }
            if (su_password.getText().equals(su_password1.getText())) {
                new_password = su_password.getText();
            }else{
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Type correct password for both text field!");
//            alert.setContentText("Type correct username and password or Sign up!");
                alert.showAndWait();
                goodData=false;
            }
        }
        if(goodData){
            String new_username = su_username.getText();
            String new_email = su_email.getText();
            User new_user = new User();
            new_user.setLogin(new_username);
            new_user.setEmail(new_email);
            new_user.setPassword(new_password);
            users_with_new_user[users.length]=new_user;
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Congratulation!");
            alert.setHeaderText(null);
            alert.setContentText("User account created successfully!");
            DBConnection dbConnection = new DBConnection();
            String hpassw="";
            try {
                hpassw=PasswordHash.hashPassword(new_password);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            String query = "INSERT INTO `users` (`id`, `username`, `password`, `email`) VALUES (NULL, '"+new_username+"', '"+hpassw+"', '"+new_email+"')";
            try (
                    Connection con = dbConnection.getConnection();
                    Statement stmt = con.createStatement();
            ) {
                stmt.executeUpdate(query);
                System.out.println("User added to database");

            } catch (SQLException e) {
                System.err.print(e);
            }

            alert.showAndWait();
            RWFile.saveNewAccount(users_with_new_user);
            showLogin();
        }
    }
}


