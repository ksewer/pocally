package sample.Controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

public class EditController {
    @FXML
    Button memoryEdit;

    public void showMemoryEdit(){
        try {
            URL url = getClass().getClassLoader().getResource("sample/Controllers/FXMLTemplates/MemoryAddWords.fxml");
            FXMLLoader loader = new FXMLLoader(url);
            Stage stage = (Stage) memoryEdit.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            stage.setScene(scene);
            MemoryAddWordsController cn = loader.getController();
            cn.showElement();
        }catch (IOException io){
            io.printStackTrace();
        }
    }

    public void showABCEdit(){
        try {
            URL url = getClass().getClassLoader().getResource("sample/Controllers/FXMLTemplates/ABCAddWords.fxml");
            FXMLLoader loader = new FXMLLoader(url);
            Stage stage = (Stage) memoryEdit.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            stage.setScene(scene);
            ABCAddWordsController cn = loader.getController();
            cn.showElement();
        }catch (IOException io){
            io.printStackTrace();
        }
    }

    public void showMixedWordsEdit(){
        try {
            URL url = getClass().getClassLoader().getResource("sample/Controllers/FXMLTemplates/MixedWordsAddWords.fxml");
            FXMLLoader loader = new FXMLLoader(url);
            Stage stage = (Stage) memoryEdit.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            stage.setScene(scene);
            Mixed_wordsAddWordsController cn = loader.getController();
            cn.showElement();
        }catch (IOException io){
            io.printStackTrace();
        }
    }

    public void showMenu(){
        try {
            URL url = getClass().getClassLoader().getResource("sample/Controllers/FXMLTemplates/Menu.fxml");
            FXMLLoader loader = new FXMLLoader(url);
            Stage stage = (Stage) memoryEdit.getScene().getWindow();

            stage.setResizable(true);
            Scene scene = new Scene(loader.load());
            stage.setScene(scene);
            ControllerMenu cm = loader.getController();
            cm.setUserInfoLabel();
        }catch (IOException io){
            io.printStackTrace();
        }
    }
}
