package sample.Controllers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import sample.RWFile;
import sample.Sentence;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Mixed_wordsAddWordsController {
    @FXML
    Button backToMenu;
    @FXML
    ChoiceBox<String> categories;
    @FXML
    ListView<String> listWords;
    @FXML
    Label numberOfWordsLabel;
    @FXML
    AnchorPane editPane;
    @FXML
    AnchorPane buttonsPane;
    @FXML
    AnchorPane newCategoryPane;
    String[] read_category;
    List<String> categories_array = new ArrayList<>();
    List<String> answers_array = new ArrayList<>();
    List<Sentence> sentences_array = new ArrayList<>();
    Sentence start = new Sentence();
    String wybrana_kategoria="Starter Pack";
    int iloscSlowek;
    Sentence[] zdania;


    public Mixed_wordsAddWordsController(){
        read_category=RWFile.ReadFile("src/db/Categories/categoriesMixed_words.txt");
        categories_array.clear();
        for(int i=0; i<read_category.length;i++){
            categories_array.add(read_category[i]);
        }
        readWordsCategory();
    }
@FXML TextField rightAnswerField;
    public void addAnswerOption(){
        ifAddNew=true;
        editPane.setVisible(true);
        buttonsPane.setVisible(false);
        rightAnswerField.setText("");
    }
    public void addAnswer(){
        String odp=rightAnswerField.getText();
        answers_array.add(odp);
        editPane.setVisible(false);
        buttonsPane.setVisible(true);
        showAnswerList();
    }

    public void confirmAddEditAnswer(){
        if(ifAddNew==true){
            addAnswer();
        }else{
            editAnswer();
        }
    }

    public void deleteAnswer(){
        choosed_position = listWords.getSelectionModel().getSelectedIndex();
        if(choosed_position==-1){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning Dialog");
            alert.setHeaderText("You have not made a choice!");
            alert.setContentText("Select the question to edit!");
            alert.showAndWait();
        }else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirmation Dialog");
            alert.setHeaderText("Are you sure that you want delete this element:");
            alert.setContentText(answers_array.get(choosed_position));

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
                answers_array.remove(choosed_position);
                showAnswerList();
            }
        }
    }

    public void editAnswerOption(){
        choosed_position = listWords.getSelectionModel().getSelectedIndex();
        if(choosed_position==-1){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning Dialog");
            alert.setHeaderText("You have not made a choice!");
            alert.setContentText("Select the question to edit!");
            alert.showAndWait();
        }else {
            ifAddNew=false;
            rightAnswerField.setText(answers_array.get(choosed_position));
            buttonsPane.setVisible(false);
            editPane.setVisible(true);
        }
    }

    public void editAnswer(){
    String odp=rightAnswerField.getText();

            answers_array.set(choosed_position,odp);
            showAnswerList();
            buttonsPane.setVisible(true);
            editPane.setVisible(false);

    }
@FXML AnchorPane addNewSentenceOption;

    public void backToMainOption(){
        buttonsPane1.setVisible(true);
        buttonsPane.setVisible(false);
        addNewSentenceOption.setVisible(false);
        editPane.setVisible(false);
        showWords();
    }

int choosed_position;
    @FXML TextField newSentenceField;
    public void addNewSentence(){
        Sentence newS= new Sentence();
        newS.pytanie=newSentenceField.getText();
        String[]odp=new String[1];
        odp[0]=newSentenceField.getText();
        newS.odpowiedzi=odp;
        sentences_array.add(newS);
        newS=null;
        showWords();
        backToMainOption();
    }

    private void saveAnswers(){
        String[] odpowiedzi=new String[answers_array.size()];
        for(int i=0; i<answers_array.size();i++){
            odpowiedzi[i]=answers_array.get(i);
        }
        sentences_array.get(choosed_position).odpowiedzi=odpowiedzi;
    }

    public void showAnswerList(){

        listWords.getItems().clear();
//        System.out.println(choosed_position);
//        Sentence edit = sentences_array.get(choosed_position);
        for(int i=0; i<answers_array.size(); i++){
            listWords.getItems().add(answers_array.get(i));
        }
        numberOfWordsLabel.setText("Number of answers: "+answers_array.size());
    }

    public void editOption(){
        answers_array.clear();
        choosed_position = listWords.getSelectionModel().getSelectedIndex();
        if(choosed_position==-1){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning Dialog");
            alert.setHeaderText("You have not made a choice!");
            alert.setContentText("Select the question to edit!");
            alert.showAndWait();
        }else {
            buttonsPane.setVisible(true);
            buttonsPane1.setVisible(false);
            for(int i=0; i<sentences_array.get(choosed_position).odpowiedzi.length; i++){
                answers_array.add(sentences_array.get(choosed_position).odpowiedzi[i]);
            }
            showAnswerList();
        }
//        String wybranePytanie=listWords.getSelectionModel().getSelectedItem();
//        if(wybranePytanie==null){
//            Alert alert = new Alert(Alert.AlertType.WARNING);
//            alert.setTitle("Warning Dialog");
//            alert.setHeaderText("Wrong choice!");
//            alert.setContentText("Select the question which you want edit!");
//            alert.showAndWait();
//        }else{
//            choosed_position=search(wybranePytanie);
//            if(choosed_position==-1){
//                ;
//            }else {
//                buttonsPane.setVisible(false);
//                editPane.setVisible(true);
//                ifAddNew = false;
//                System.out.println(choosed_position);
//                questionField.setText(question_array.get(choosed_position)[0]);
//                ansAField.setText(question_array.get(choosed_position)[1]);
//                ansBField.setText(question_array.get(choosed_position)[2]);
//                ansCField.setText(question_array.get(choosed_position)[3]);
//                rightAnswer.setValue(changeRightAnswerToString(question_array.get(choosed_position)[4]));
//            }}
    }

    public void backWithSaveAnswers(){
        saveAnswers();
        backToMainOption();
    }

    public void deleteSentence(){
        choosed_position = listWords.getSelectionModel().getSelectedIndex();

        if(choosed_position>-1) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirmation Dialog");
            alert.setHeaderText("Are you sure that you want delete this element:");
            alert.setContentText(sentences_array.get(choosed_position).pytanie);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
                sentences_array.remove(choosed_position);
                showWords();
            }
        }

    }


    @FXML TextField nameNewCat;
    @FXML AnchorPane buttonsPane1;

    boolean ifAddNew=false;
    public void addOption(){
        buttonsPane1.setVisible(false);
        addNewSentenceOption.setVisible(true);
        newSentenceField.setText("");
       // buttonsPane.setVisible(true);
        ifAddNew=true;
//        questionField.setText("");
//        ansAField.setText("");
//        ansBField.setText("");
//        ansCField.setText("");
//        rightAnswer.setValue("");
    }

    private void convertTabToArray(){
        for(int i=0; i<iloscSlowek;i++){
            sentences_array.add(zdania[i]);
        }
    }

    private void readWordsCategory(){
        start.odczytajZdania("src/db/Mixed_words/"+wybrana_kategoria+".txt");
        zdania=start.getZdania();
        iloscSlowek=zdania.length;
        convertTabToArray();
    }

    public void saveOption(){
        saveAnswers();
        List<String> sentenceToSave = new ArrayList<>();
        for(int i=0;i<sentences_array.size();i++){
            sentenceToSave.add("next");
            sentenceToSave.add(sentences_array.get(i).pytanie);
            for(int j=0; j<sentences_array.get(i).odpowiedzi.length;j++){
                sentenceToSave.add(sentences_array.get(i).odpowiedzi[j]);
            }
        }
        String [] sentenceToSaveTab = new String[sentenceToSave.size()];
        for(int i=0; i<sentenceToSave.size();i++){
            sentenceToSaveTab[i]=sentenceToSave.get(i);
        }
        RWFile.saveDataBase(sentenceToSaveTab,"src/db/Mixed_words/"+wybrana_kategoria+".txt");
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        alert.setContentText("I have a great message for you! You saved the database correctly.");

        alert.showAndWait();
    }
    public void deleteCategory() {
        choosed_position = listWords.getSelectionModel().getSelectedIndex();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText("Are you sure that you want delete this element:");
        alert.setContentText(categories_array.get(choosed_position));

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            RWFile.deleteFile("src/db/Mixed_words/" + categories_array.get(choosed_position) + ".txt");
            categories_array.remove(choosed_position);
            saveCategories();
            showCategories();
        }
    }

    private void saveCategories(){
        String[] categoriesToSave=new String[categories_array.size()];
        for(int i=0;i<categories_array.size();i++) {
            categoriesToSave[i]=categories_array.get(i);
            System.out.println("kategoria "+i+" "+categoriesToSave[i]);
        }
        RWFile.saveDataBase(categoriesToSave,"src/db/Categories/categoriesMixed_words.txt");
    }

    private void alertSaveQuestion(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText("Do you want to save?");
        alert.setContentText("Choose your option.");

        ButtonType buttonTypeYES = new ButtonType("YES");
        ButtonType buttonTypeNO = new ButtonType("NO");
        //ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);

        alert.getButtonTypes().setAll(buttonTypeYES, buttonTypeNO);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeYES){
            //saveOption();
        }
    }

    private void showCategories(){
        listWords.getItems().clear();
        listWords.getItems().add(categories_array.get(0));
        //System.out.println(categories_array.get(0));
        int il_kat=categories.getItems().size();
        //System.out.println(il_kat);
        for (int i=1;i<categories_array.size();i++){
            if(i<il_kat){
                categories.getItems().set(i,categories_array.get(i));
            }else{
                categories.getItems().add(categories_array.get(i));
            }
            listWords.getItems().add(categories_array.get(i));
        }
        categories.setValue(categories_array.get(0));
    }

    public void showElement(){
        numberOfWordsLabel.setText("Number of questions: "+iloscSlowek);
        categories.setStyle("-fx-font: 25px \"Impact\";");
        for(int i=0; i<read_category.length;i++){
            categories.getItems().add(read_category[i]);
        }
        categories.setValue(read_category[0]);

        categories.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number number2) {
                wybrana_kategoria=categories.getItems().get((Integer) number2);
                System.out.println(wybrana_kategoria);
                listWords.getItems().clear();
                readWordsCategory();
                sentences_array.clear();
                if(zdania.length>0){
                    convertTabToArray();
                    showWords();
                }
                numberOfWordsLabel.setText("Number of questions: "+sentences_array.size());
            }
        });
//        readWordsCategory();
//        convertTabToArray();
        showWords();
    }

    public void addCategory(){
        boolean ifRepeat=false;
        String name=nameNewCat.getText();
        if(name.equals("")){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning Dialog");
            alert.setHeaderText("Text Field is empty!");
            alert.setContentText("I can't add/modify empty category!");

            alert.showAndWait();
        }else{
            nameNewCat.setText("");
            for(int i=0; i<categories_array.size();i++){
                if(categories_array.get(i).equals(name)){
                    ifRepeat=true;
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Warning Dialog");
                    alert.setHeaderText("The category "+name+" already exist");
                    alert.setContentText("I can't add this category!");

                    alert.showAndWait();
                    break;
                }
            }
            if(ifRepeat==false){
                categories_array.add(name);
                showCategories();
                RWFile.createNewFile("src/db/Mixed_words/"+name+".txt");
                saveCategories();
            }
        }
    }

    public void addCategoryOption(){
        read_category=RWFile.ReadFile("src/db/Categories/categoriesMixed_words.txt");
        buttonsPane.setVisible(false);
        buttonsPane1.setVisible(false);
        newCategoryPane.setVisible(true);
        listWords.getItems().clear();
        nameNewCat.setText("");
        numberOfWordsLabel.setVisible(false);

        for(int i=0;i<read_category.length;i++){
            System.out.println(read_category[i]);
            listWords.getItems().add(read_category[i]);
            //categories_array.add(read_category[i]);
        }
    }

    public void backToAnswerOption(){
        buttonsPane.setVisible(true);
        editPane.setVisible(false);
    }

    private void showWords(){
        listWords.getItems().clear();
        iloscSlowek=sentences_array.size();
        for(int i=0; i<iloscSlowek;i++){
            listWords.getItems().add(sentences_array.get(i).pytanie);
        }
        numberOfWordsLabel.setText("Number of sentences: "+iloscSlowek);
    }

    public void showMenu(){
        alertSaveQuestion();
        try {
            URL url = getClass().getClassLoader().getResource("sample/Controllers/FXMLTemplates/Menu.fxml");
            FXMLLoader loader = new FXMLLoader(url);
            Stage stage = (Stage) backToMenu.getScene().getWindow();

            stage.setResizable(true);
            Scene scene = new Scene(loader.load());
            stage.setScene(scene);
            ControllerMenu cm = loader.getController();
            cm.setUserInfoLabel();
        }catch (IOException io){
            io.printStackTrace();
        }
    }
}
