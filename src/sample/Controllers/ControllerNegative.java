package sample.Controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import sample.Sentence;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLOutput;
import java.util.Random;

public class ControllerNegative {

    Sentence[] zdania;
    Sentence start = new Sentence();
    Random generator=new Random();
    int [] pytania_wylosowane= new int[3];
    int wylosowane_pytanie;

    @FXML
    Label q1;
    @FXML
    Label q2;
    @FXML
    Label q3;
    @FXML
    TextField answer_1;
    @FXML
    TextField answer_2;
    @FXML
    TextField answer_3;
    @FXML Label totalScoreLabel;
    @FXML Label wybranyCzasLabel;
    TextField tab_answer[]=new TextField[3];

    public ControllerNegative(){
        start.odczytajZdania("src/db/"+ControllerMenu.wybrany_czas+"/przeczenie.txt");
        zdania=start.getZdania();

        for(int i=0;i<3;i++){
            pytania_wylosowane[i]=0;
        }
        boolean czyRozneLosowe=true;
        do{
            czyRozneLosowe=true;
            for(int i=0; i<3;i++){
                wylosowane_pytanie=generator.nextInt(Sentence.ilosc_zdan);
                for(int j=0;j<3;j++){
                    if(wylosowane_pytanie==pytania_wylosowane[j]){
                        wylosowane_pytanie=generator.nextInt(Sentence.ilosc_zdan);
                        czyRozneLosowe=false;
                    }
                }
                pytania_wylosowane[i]=wylosowane_pytanie;
            }
        }while(czyRozneLosowe);

    }

    public void showSentence(){
        for(int i=0; i<3; i++){
            zdania[pytania_wylosowane[i]].pytanie=zdania[pytania_wylosowane[i]].pytanie.trim();
            System.out.println(zdania[pytania_wylosowane[i]].pytanie);
            System.out.println(zdania[pytania_wylosowane[i]].odpowiedzi[0]);
        }
        totalScoreLabel.setText(Integer.toString(ControllerMenu.totalScore));
        wybranyCzasLabel.setText(ControllerMenu.wybrany_czas);
        q1.setText(zdania[pytania_wylosowane[0]].pytanie);
        q2.setText(zdania[pytania_wylosowane[1]].pytanie);
        q3.setText(zdania[pytania_wylosowane[2]].pytanie);

    }
@FXML
Button checkButton;
    public void check(){
        checkButton.setVisible(false);
        String[]answer=new String[3];
        answer[0]=answer_1.getText().trim();
        answer[1]=answer_2.getText().trim();
        answer[2]=answer_3.getText().trim();
        boolean czyPrawidlowaOdp=false;
        tab_answer[0]=answer_1;
        tab_answer[1]=answer_2;
        tab_answer[2]=answer_3;
        for(int i=0;i<3;i++) {
            czyPrawidlowaOdp=false;
            for (int j = 0; j < zdania[pytania_wylosowane[i]].odpowiedzi.length; j++) {
                if(zdania[pytania_wylosowane[i]].odpowiedzi[j].toUpperCase().equals(answer[i].toUpperCase())){
                    System.out.println("GOOD "+i);
                    ControllerMenu.totalScore++;
                    tab_answer[i].setBackground(new Background(new BackgroundFill(Color.GREEN, new CornerRadii(3), Insets.EMPTY)));
                    czyPrawidlowaOdp=true;
                }
            }
            if(czyPrawidlowaOdp==false){
                tab_answer[i].setBackground(new Background(new BackgroundFill(Color.RED, new CornerRadii(3), Insets.EMPTY)));
            }
        }
        totalScoreLabel.setText(Integer.toString(ControllerMenu.totalScore));
    }


    public void showMenu(){
        try {
            URL url = getClass().getClassLoader().getResource("sample/Controllers/FXMLTemplates/Menu.fxml");
            FXMLLoader loader = new FXMLLoader(url);
            Stage stage = (Stage) q1.getScene().getWindow();

            stage.setResizable(true);
            Scene scene = new Scene(loader.load());
            stage.setScene(scene);
            ControllerMenu cm = loader.getController();
            cm.setUserInfoLabel();

        }catch (IOException io){
            io.printStackTrace();
        }
    }
}
