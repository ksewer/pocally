package sample.Controllers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import sample.Sentence;

import java.io.IOException;
import java.net.URL;
import java.util.Random;



public class Controller  {
    public Button btn_msg;
    public HBox btn_puste;
    public HBox btn_wyrazy;
    private String napis_z_przycisku="xddd";
    private int licznik_wyrazow=1;
    private Button [] przycisk;
    private Button [] przyciski_puste;
    //private String[] pobrane_zadanie;
    private String sprawdzany_wyraz;
    Random generator=new Random();
    int wylosowany_wyraz_w_tablicy=0;
    Sentence start=new Sentence();
    Sentence[] zdania;

    @FXML
    Label totalScoreLabel;
    @FXML Label wybranyCzasLabel;

    public Controller(){


    }
    public void pressButton(String category) {
        start.odczytajZdania("src/db/Mixed_words/"+category+".txt");
        zdania=start.getZdania();
        wylosowany_wyraz_w_tablicy=generator.nextInt(Sentence.ilosc_zdan);
        zdania[wylosowany_wyraz_w_tablicy].pytanie=zdania[wylosowany_wyraz_w_tablicy].pytanie.trim();
        System.out.println(zdania[wylosowany_wyraz_w_tablicy].pytanie);

        for(int i=0; i<zdania[wylosowany_wyraz_w_tablicy].pytanie.length(); i++){
            if(zdania[wylosowany_wyraz_w_tablicy].pytanie.charAt(i)==' ')licznik_wyrazow++;
        }

        String wyrazy[] = new String[licznik_wyrazow];
        wyrazy=zdania[wylosowany_wyraz_w_tablicy].pytanie.split(" ");
        przycisk=new Button[licznik_wyrazow];
        przyciski_puste = new Button[licznik_wyrazow];
        String wyrazy_pomieszane[]=new String[licznik_wyrazow];
        int wylosowana_liczba;

        for(int i=0; i<licznik_wyrazow;i++) {
            przyciski_puste[i]=new Button();
            Button przycisk_wysylany=przyciski_puste[i];
            przyciski_puste[i].setMinSize(35,10);
            przyciski_puste[i].setFont(Font.font ("Impact", 20));
            przyciski_puste[i].setOnMouseDragOver(new EventHandler<MouseDragEvent>() {
                @Override
                public void handle(MouseDragEvent event) {
                    sprawdzPrzyciskPusty(przycisk_wysylany);
                }
            });
            przyciski_puste[i].setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    sprawdzPrzyciskPusty(przycisk_wysylany);
                }
            });

            btn_puste.getChildren().addAll(przyciski_puste[i]);
            totalScoreLabel.setText(Integer.toString(ControllerMenu.totalScore));
            wybranyCzasLabel.setText(category);
        }


        for(int i=0; i<licznik_wyrazow;i++) {
            wyrazy_pomieszane[i] = "";
        }


        for (int i=0; i<licznik_wyrazow; i++)
        {
            wylosowana_liczba=generator.nextInt(licznik_wyrazow);
            while(!wyrazy_pomieszane[wylosowana_liczba].equals("")){
                wylosowana_liczba=generator.nextInt(licznik_wyrazow);
            }
            wyrazy_pomieszane[wylosowana_liczba]=wyrazy[i];
        }


        for(int i=0;i<licznik_wyrazow; i++)
        {
            przycisk[i]=new Button();
            Button przycisk_wysylany=przycisk[i];
            przycisk[i].setFont(Font.font ("Impact", 20));
            przycisk[i].setOnMouseDragged(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    sprawdzPrzyciskZNapisem(przycisk_wysylany);
                }
            });
            przycisk[i].setText(wyrazy_pomieszane[i]);
            przycisk[i].setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    sprawdzPrzyciskZNapisem(przycisk_wysylany);
                }
            });
            btn_wyrazy.getChildren().addAll(przycisk[i]);
        }
    }

    public void sprawdzPrzyciskZNapisem(Button przycisk){
        napis_z_przycisku=przycisk.getText();
    }

    public void sprawdzPrzyciskPusty(Button przyciskw){
        if(napis_z_przycisku.equals("xddd")){
            for(int i=0; i<licznik_wyrazow; i++){
                if(przyciskw.getText().equals(przycisk[i].getText())){
                    przycisk[i].setVisible(true);
                }
            }
            przyciskw.setText("");
        }else {
            if (przyciskw.getText().equals("")) {
                przyciskw.setText(napis_z_przycisku);
                for (int i = 0; i < licznik_wyrazow; i++) {
                    if (napis_z_przycisku.equals(przycisk[i].getText())) {
                        przycisk[i].setVisible(false);
                        napis_z_przycisku = "xddd";
                    }
                }

            } else {
                for (int i = 0; i < licznik_wyrazow; i++) {
                    if (przyciskw.getText().equals(przycisk[i].getText())) {
                        przycisk[i].setVisible(true);
                    }
                }
                przyciskw.setText(napis_z_przycisku);
                for (int i = 0; i < licznik_wyrazow; i++) {
                    if (napis_z_przycisku.equals(przycisk[i].getText())) {
                        przycisk[i].setVisible(false);
                        napis_z_przycisku = "xddd";
                    }

                }
            }
        }
    }
@FXML Button checkButton;
    public void checkAnswer(){
        checkButton.setVisible(false);
        sprawdzany_wyraz="";
        for(int i=0; i<licznik_wyrazow;i++){
            if(i<licznik_wyrazow-1){
                sprawdzany_wyraz=sprawdzany_wyraz+przyciski_puste[i].getText()+" ";
            }
            else sprawdzany_wyraz=sprawdzany_wyraz+przyciski_puste[i].getText();
        }
        for(int j=0;j<zdania[wylosowany_wyraz_w_tablicy].odpowiedzi.length;j++){
            if((sprawdzany_wyraz.toUpperCase()).equals(zdania[wylosowany_wyraz_w_tablicy].odpowiedzi[j].toUpperCase())){
                for(int i=0; i<licznik_wyrazow;i++){
                    przyciski_puste[i].setBackground(new Background(new BackgroundFill(Color.GREEN, new CornerRadii(3), Insets.EMPTY)));
                    przyciski_puste[i].setTextFill(Color.AQUA);
                    //przyciski_puste[i].setBorder(new Border(new BorderStroke(Color.GREEN, BorderStrokeStyle.DOTTED, new CornerRadii(5), new BorderWidths(3))));
                }
                ControllerMenu.totalScore++;
                ControllerMenu.totalScore++;
                totalScoreLabel.setText(Integer.toString(ControllerMenu.totalScore));
            }
        }
    }

    @FXML Button backButton;

    public void showMenu(){
        try {
            URL url = getClass().getClassLoader().getResource("sample/Controllers/FXMLTemplates/Menu.fxml");
            FXMLLoader loader = new FXMLLoader(url);
            Stage stage = (Stage) backButton.getScene().getWindow();

            stage.setResizable(true);
            Scene scene = new Scene(loader.load());
            stage.setScene(scene);
            ControllerMenu cm = loader.getController();
            cm.setUserInfoLabel();

        }catch (IOException io){
            io.printStackTrace();
        }
    }

    @FXML
    AnchorPane RozsypankaPane;
}
