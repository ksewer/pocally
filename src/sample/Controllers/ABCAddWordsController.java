package sample.Controllers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import sample.RWFile;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ABCAddWordsController {

    String[] read_category;
    List<String> categories_array = new ArrayList<>();
    String[] pobranePytania;
    String wybrana_kategoria="Starter Pack";
    String[][] pytaniaPosortowane;
    int iloscSlowek;
    List<String[]> question_array = new ArrayList<String[]>();
    int choosed_position;

    public ABCAddWordsController(){
        read_category=RWFile.ReadFile("src/db/Categories/categoriesABC.txt");
        categories_array.clear();
        for(int i=0; i<read_category.length;i++){
            categories_array.add(read_category[i]);
        }
        readWordsCategory();
    }

    private void readWordsCategory(){
        pobranePytania= RWFile.ReadFile("src/db/ABC/"+wybrana_kategoria+".txt");
        iloscSlowek=pobranePytania.length/2;
        pytaniaPosortowane=new String[iloscSlowek][5];
        int k=0;
        String [] splitedArray;
        for(int i=0; i<iloscSlowek;i++){
            pytaniaPosortowane[i][0]=pobranePytania[k++];
            splitedArray = pobranePytania[k++].split(",");
            pytaniaPosortowane[i][1]=splitedArray[0];
            pytaniaPosortowane[i][2]=splitedArray[1];
            pytaniaPosortowane[i][3]=splitedArray[2];
            pytaniaPosortowane[i][4]=splitedArray[3];
        }
    }


    @FXML
    Button backToMenu;
    @FXML
    ChoiceBox<String> categories;
    @FXML
    ChoiceBox<String> rightAnswer;
    @FXML
    ListView<String> listWords;
    @FXML
    Label numberOfWordsLabel;
    @FXML
    AnchorPane editPane;
    @FXML
    AnchorPane buttonsPane;
    @FXML
    AnchorPane newCategoryPane;



    private void convertTabToArray(){
        for(int i=0; i<iloscSlowek;i++){
            question_array.add(pytaniaPosortowane[i]);
        }
    }

    private void showWords(){
        listWords.getItems().clear();
        iloscSlowek=question_array.size();
        for(int i=0; i<iloscSlowek;i++){
            listWords.getItems().add("q: "+question_array.get(i)[0]);
            listWords.getItems().add("a: "+question_array.get(i)[1]);
            listWords.getItems().add("b: "+question_array.get(i)[2]);
            listWords.getItems().add("c: "+question_array.get(i)[3]);
            listWords.getItems().add("right answer: "+changeRightAnswerToString(question_array.get(i)[4]));
        }
        numberOfWordsLabel.setText("Number of questions: "+iloscSlowek);
    }


    public void showElement(){
        rightAnswer.getItems().add("A");
        rightAnswer.getItems().add("B");
        rightAnswer.getItems().add("C");
        categories.setStyle("-fx-font: 25px \"Impact\";");
        for(int i=0; i<read_category.length;i++){
            categories.getItems().add(read_category[i]);
        }
        categories.setValue(read_category[0]);

        categories.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number number2) {
                wybrana_kategoria=categories.getItems().get((Integer) number2);
                System.out.println(wybrana_kategoria);
                listWords.getItems().clear();
                readWordsCategory();
                question_array.clear();
                if(pobranePytania.length>0){
                    convertTabToArray();
                    showWords();
                }
                numberOfWordsLabel.setText("Number of questions: "+question_array.size());
            }
        });
        readWordsCategory();
        convertTabToArray();
        showWords();
    }
    @FXML TextField nameNewCat;
    public void addCategoryOption(){
        read_category=RWFile.ReadFile("src/db/Categories/categoriesABC.txt");
        buttonsPane.setVisible(false);
        newCategoryPane.setVisible(true);
        listWords.getItems().clear();
        nameNewCat.setText("");
        numberOfWordsLabel.setVisible(false);

        for(int i=0;i<read_category.length;i++){
            System.out.println(read_category[i]);
            listWords.getItems().add(read_category[i]);
            //categories_array.add(read_category[i]);
        }
    }

    boolean ifAddNew=false;

    public void addOption(){
        buttonsPane.setVisible(false);
        editPane.setVisible(true);
        ifAddNew=true;
        questionField.setText("");
        ansAField.setText("");
        ansBField.setText("");
        ansCField.setText("");
        rightAnswer.setValue("");
    }

    private String changeRightAnswerToString(String numerOdp){
        if(numerOdp.equals("1"))return "A";
        else if(numerOdp.equals("2"))return "B";
        else return "C";
    }

    private String changeRightAnswerToInt(String odp){
        if(odp.equals("A"))return "1";
        else if(odp.equals("B"))return "2";
        else return "3";
    }

    @FXML TextField questionField;
    @FXML TextField ansAField;
    @FXML TextField ansBField;
    @FXML TextField ansCField;

    public void editOption(){
        String wybranePytanie=listWords.getSelectionModel().getSelectedItem();
        if(wybranePytanie==null){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning Dialog");
            alert.setHeaderText("Wrong choice!");
            alert.setContentText("Select the question which you want edit!");
            alert.showAndWait();
        }else{
        choosed_position=search(wybranePytanie);
        if(choosed_position==-1){
            ;
        }else {
            buttonsPane.setVisible(false);
            editPane.setVisible(true);
            ifAddNew = false;
            System.out.println(choosed_position);
            questionField.setText(question_array.get(choosed_position)[0]);
            ansAField.setText(question_array.get(choosed_position)[1]);
            ansBField.setText(question_array.get(choosed_position)[2]);
            ansCField.setText(question_array.get(choosed_position)[3]);
            rightAnswer.setValue(changeRightAnswerToString(question_array.get(choosed_position)[4]));
        }}
    }

    public void confirmAddEdit(){
        String[] questionTemporary=new String[5];
        questionTemporary[0]=questionField.getText();
        questionTemporary[1]=ansAField.getText();
        questionTemporary[2]=ansBField.getText();
        questionTemporary[3]=ansCField.getText();
        questionTemporary[4]=changeRightAnswerToInt(rightAnswer.getValue());
        if(questionField.getText().equals("")||ansAField.getText().equals("")||ansBField.getText().equals("")||ansCField.getText().equals("")||rightAnswer.getValue().equals("")){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning Dialog");
            alert.setHeaderText("Text Field is empty!");
            alert.setContentText("I can't add/modify empty word!");

            alert.showAndWait();
        }else{
            questionField.setText("");
            ansAField.setText("");
            ansBField.setText("");
            ansCField.setText("");
            rightAnswer.setValue("");
            if(ifAddNew==true){
                question_array.add(questionTemporary);
                showWords();
            }else{
                question_array.set(choosed_position,questionTemporary);
                showWords();
            }
            backToOption();
        }
    }

    public void deleteCategory() {
        choosed_position = listWords.getSelectionModel().getSelectedIndex();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText("Are you sure that you want delete this element:");
        alert.setContentText(categories_array.get(choosed_position));

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            RWFile.deleteFile("src/db/ABC/" + categories_array.get(choosed_position) + ".txt");
            categories_array.remove(choosed_position);
            saveCategories();
            showCategories();
        }
    }

    private void showCategories(){
        listWords.getItems().clear();
        listWords.getItems().add(categories_array.get(0));
        //System.out.println(categories_array.get(0));
        int il_kat=categories.getItems().size();
        //System.out.println(il_kat);
        for (int i=1;i<categories_array.size();i++){
            if(i<il_kat){
                categories.getItems().set(i,categories_array.get(i));
            }else{
                categories.getItems().add(categories_array.get(i));
            }
            listWords.getItems().add(categories_array.get(i));
        }
        categories.setValue(categories_array.get(0));

    }

    public void addCategory(){
        boolean ifRepeat=false;
        String name=nameNewCat.getText();
        if(name.equals("")){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning Dialog");
            alert.setHeaderText("Text Field is empty!");
            alert.setContentText("I can't add/modify empty category!");

            alert.showAndWait();
        }else{
            nameNewCat.setText("");
            for(int i=0; i<categories_array.size();i++){
                if(categories_array.get(i).equals(name)){
                    ifRepeat=true;
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Warning Dialog");
                    alert.setHeaderText("The category "+name+" already exist");
                    alert.setContentText("I can't add this category!");

                    alert.showAndWait();
                    break;
                }
            }
            if(ifRepeat==false){
                categories_array.add(name);
                showCategories();
                RWFile.createNewFile("src/db/ABC/"+name+".txt");
                saveCategories();
            }
        }
    }

    public void backToOption(){
        //alertSaveQuestion();
        numberOfWordsLabel.setVisible(true);
        buttonsPane.setVisible(true);
        editPane.setVisible(false);
        newCategoryPane.setVisible(false);
        showWords();
    }

    private void alertSaveQuestion(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText("Do you want to save?");
        alert.setContentText("Choose your option.");

        ButtonType buttonTypeYES = new ButtonType("YES");
        ButtonType buttonTypeNO = new ButtonType("NO");
        //ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);

        alert.getButtonTypes().setAll(buttonTypeYES, buttonTypeNO);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeYES){
            //saveOption();
        }
    }

    String [] pytaniaDoZapisu;

    private void convertArrayToTab(){
    pytaniaDoZapisu=new String[question_array.size()*2];
    int k=0;
    String laczonyNapis;
    for(int i=0;i<question_array.size();i++){
        laczonyNapis="";
        pytaniaDoZapisu[k++]=question_array.get(i)[0];
        laczonyNapis+=question_array.get(i)[1]+","+question_array.get(i)[2]+","+question_array.get(i)[3]+","+question_array.get(i)[4];
        pytaniaDoZapisu[k++]=laczonyNapis;
    }

    }

    public void saveOption(){
        convertArrayToTab();
        RWFile.saveDataBase(pytaniaDoZapisu,"src/db/ABC/"+wybrana_kategoria+".txt");
        saveCategories();

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        alert.setContentText("I have a great message for you! You saved the database correctly.");

        alert.showAndWait();
    }

    private void saveCategories(){
        String[] categoriesToSave=new String[categories_array.size()];
        for(int i=0;i<categories_array.size();i++) {
            categoriesToSave[i]=categories_array.get(i);
            System.out.println("kategoria "+i+" "+categoriesToSave[i]);
        }
        RWFile.saveDataBase(categoriesToSave,"src/db/Categories/categoriesABC.txt");
    }

    private int search(String wyraz){
        int znalezionyWyraz=-1;
        String[] pytanie=wyraz.split(": ");
        if(!pytanie[0].equals("q")){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning Dialog");
            alert.setHeaderText("Wrong choice!");
            alert.setContentText("Select the question which you want edit!");
            alert.showAndWait();
        }else {
            for (int i = 0; i < question_array.size(); i++) {
                if (question_array.get(i)[0].equals(pytanie[1])) {
                    znalezionyWyraz = i;
                    break;
                }
            }
        }
        return znalezionyWyraz;
    }

    public void deleteQuestion(){
        String wybranePytanie=listWords.getSelectionModel().getSelectedItem();
        choosed_position=search(wybranePytanie);
        if(choosed_position>-1) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText("Are you sure that you want delete this element:");
        alert.setContentText(question_array.get(choosed_position)[0]);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            question_array.remove(choosed_position);
            showWords();
        }
        }

    }

    public void showMenu(){
        alertSaveQuestion();
        try {
            URL url = getClass().getClassLoader().getResource("sample/Controllers/FXMLTemplates/Menu.fxml");
            FXMLLoader loader = new FXMLLoader(url);
            Stage stage = (Stage) backToMenu.getScene().getWindow();

            stage.setResizable(true);
            Scene scene = new Scene(loader.load());
            stage.setScene(scene);
            ControllerMenu cm = loader.getController();
            cm.setUserInfoLabel();
        }catch (IOException io){
            io.printStackTrace();
        }
    }
}
