package sample.Controllers;


import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import sample.RWFile;
import sample.User;
import sample.Wisielec;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ControllerMenu  {

    public Button btn_option1;
    public Button btn_option2;
    public Button saveButton;
    public static String[] kategorieMemory;
    public static boolean wisielecOpen=false;
    public Button btn_option4;
    public static String nick;
    public static String email;
    public static int totalScore;
    public static String wybrany_czas="Present Simple";
    Wisielec hangman;
    @FXML AnchorPane rootPane;
    @FXML
    Label nickLabel;
    @FXML Label emailLabel;
    @FXML Label totalScoreLabel;
    @FXML Button btn_option5;
    @FXML
    ChoiceBox<String> wybor_czasu;

    public void setUserInfo(User user){
    nick=user.getLogin();
    email=user.getEmail();
    totalScore=user.score;

    setUserInfoLabel();
    }

    public void setUserInfoLabel(){
        nickLabel.setText(nick);
        emailLabel.setText(email);
        totalScoreLabel.setText(Integer.toString(ControllerMenu.totalScore));
        wybor_czasu.setStyle("-fx-font: 18px \"Impact\";");
        wybor_czasu.getItems().add("Present Simple");
        wybor_czasu.getItems().add("Present Continuous");

        wybor_czasu.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number number2) {
                wybrany_czas=wybor_czasu.getItems().get((Integer) number2);
                System.out.println(wybrany_czas);
            }
        });
        wybor_czasu.setValue(wybrany_czas);
    }

    public void pokazRozsypanka(){
        String category="Starter Pack";
        kategorieMemory=RWFile.ReadFile("src/db/Categories/categoriesMixed_words.txt");
        List<String> choices = new ArrayList<>();
        for(int i=0;i<kategorieMemory.length;i++){
            choices.add(kategorieMemory[i]);
        }

        ChoiceDialog<String> dialog = new ChoiceDialog<>(kategorieMemory[0], choices);
        dialog.setTitle("Mixed words");
        dialog.setHeaderText("Choose the category:");
        dialog.setContentText("");


// Traditional way to get the response value.
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()){
            category=result.get();
            System.out.println("Your choice: " + category);
        }

// The Java 8 way to get the response value (with lambda expression).
        result.ifPresent(letter -> System.out.println("Your choice: " + letter));
        try {
            URL url = getClass().getClassLoader().getResource("sample/Controllers/FXMLTemplates/sample.fxml");
            FXMLLoader loader = new FXMLLoader(url);
            Stage stage = (Stage) btn_option1.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            stage.setScene(scene);
            Controller ct = loader.getController();
            ct.pressButton(category);
        }catch (IOException io){
            io.printStackTrace();
        }
    }
    public static String category;
    public void pokazCTABC(){
        category="Starter Pack";
        kategorieMemory=RWFile.ReadFile("src/db/Categories/categoriesABC.txt");
        List<String> choices = new ArrayList<>();
        for(int i=0;i<kategorieMemory.length;i++){
            choices.add(kategorieMemory[i]);
        }

        ChoiceDialog<String> dialog = new ChoiceDialog<>(kategorieMemory[0], choices);
        dialog.setTitle("ABC");
        dialog.setHeaderText("Choose the category:");
        dialog.setContentText("");


// Traditional way to get the response value.
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()){
            category=result.get();
            System.out.println("Your choice: " + category);
        }

// The Java 8 way to get the response value (with lambda expression).
        result.ifPresent(letter -> System.out.println("Your choice: " + letter));
        try {
            URL url = getClass().getClassLoader().getResource("sample/Controllers/FXMLTemplates/CompleteTextABC.fxml");
            FXMLLoader loader = new FXMLLoader(url);
            Stage stage = (Stage) btn_option4.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            stage.setScene(scene);
            ControllerCTABC ct = loader.getController();
            //ct.readWords(category);
        }catch (IOException io){
            io.printStackTrace();
        }
    }

//    public void pokazCT(){
//        try {
//            FXMLLoader loader = new FXMLLoader(getClass().getResource("./FXMLTemplates/CompleteText.fxml"));
//            Stage stage = (Stage) btn_option2.getScene().getWindow();
//            Scene scene = new Scene(loader.load());
//            stage.setScene(scene);
//        }catch (IOException io){
//            io.printStackTrace();
//        }
//
//    }

    public void pokazWisielec(){
        category="Starter Pack";
        kategorieMemory=RWFile.ReadFile("src/db/Categories/categoriesHangman.txt");
        List<String> choices = new ArrayList<>();
        for(int i=0;i<kategorieMemory.length;i++){
            choices.add(kategorieMemory[i]);
        }

        ChoiceDialog<String> dialog = new ChoiceDialog<>(kategorieMemory[0], choices);
        dialog.setTitle("HANGMAN");
        dialog.setHeaderText("Choose the category:");
        dialog.setContentText("");


// Traditional way to get the response value.
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()){
            category=result.get();
            System.out.println("Your choice: " + category);
        }

// The Java 8 way to get the response value (with lambda expression).
        result.ifPresent(letter -> System.out.println("Your choice: " + letter));
        hangman = new Wisielec();
        hangman.pokazWisielec(hangman);
        hangman.przypiszDaneStartoweWisielec();
        wisielecOpen=true;
        do{
            totalScoreLabel.setText(Integer.toString(ControllerMenu.totalScore));
        }while(wisielecOpen);
    }

    public void showEdit(){
        try {
            URL url = getClass().getClassLoader().getResource("sample/Controllers/FXMLTemplates/Edit.fxml");
            FXMLLoader loader = new FXMLLoader(url);
            Stage stage = (Stage) btn_option5.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            stage.setScene(scene);
            EditController cn = loader.getController();
            //cn.showSentence();
        }catch (IOException io){
            io.printStackTrace();
        }
    }

    public void showNegative(){
        try {
            URL url = getClass().getClassLoader().getResource("sample/Controllers/FXMLTemplates/Negative.fxml");
            FXMLLoader loader = new FXMLLoader(url);
            Stage stage = (Stage) btn_option5.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            stage.setScene(scene);
            ControllerNegative cn = loader.getController();
            cn.showSentence();
        }catch (IOException io){
            io.printStackTrace();
        }

    }

    @FXML Button btn_option6;

    public void showMemory(){
        String category="Starter Pack";
        kategorieMemory=RWFile.ReadFile("src/db/Categories/categoriesMemory.txt");
        List<String> choices = new ArrayList<>();
        for(int i=0;i<kategorieMemory.length;i++){
            choices.add(kategorieMemory[i]);
        }

        ChoiceDialog<String> dialog = new ChoiceDialog<>(kategorieMemory[0], choices);
        dialog.setTitle("Memory game");
        dialog.setHeaderText("Choose the category:");
        dialog.setContentText("");


// Traditional way to get the response value.
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()){
            category=result.get();
            System.out.println("Your choice: " + category);
        }

// The Java 8 way to get the response value (with lambda expression).
        result.ifPresent(letter -> System.out.println("Your choice: " + letter));
        ControllerMemory.memoryRun=true;
        try {
            URL url = getClass().getClassLoader().getResource("sample/Controllers/FXMLTemplates/Memory.fxml");
            FXMLLoader loader = new FXMLLoader(url);
            Stage stage = (Stage) btn_option6.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            stage.setScene(scene);
            ControllerMemory ct = loader.getController();
            ct.addElementToGrid(category);
            stage.setOnHiding(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            System.exit(0);
                        }
                    });
                }
            });
        }catch (IOException io){
            io.printStackTrace();
        }
    }

    public void showLogin(){
        saveProgress();

        try {
            URL url = getClass().getClassLoader().getResource("sample/Controllers/FXMLTemplates/Login.fxml");
            FXMLLoader loader = new FXMLLoader(url);
            Stage stage = (Stage) emailLabel.getScene().getWindow();

            stage.setResizable(true);
            Scene scene = new Scene(loader.load());
            stage.setScene(scene);
            //przechwycony=loader.getController();
            //setWindowMenuSize(600,600);

        }catch (IOException io){
            io.printStackTrace();
        }
    }

    public void saveProgress(){
        LoginController.users[LoginController.user_login].score=totalScore;
        RWFile.saveNewAccount(LoginController.users);
    }


}
