package sample.Controllers;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import sample.RWFile;
import sample.Timer;

import java.io.IOException;
import java.net.URL;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class ControllerMemory {
    Image imagePytajnik = new Image(getClass().getResourceAsStream("/memory_pytajnik.png"));
    int ilosc_wyrazow=16;
    public static boolean memoryRun=true;
    Label[] words = new Label[ilosc_wyrazow];
    String[][] wordsToShow = new String[8][2];
    String[][] wordsAll;
    String[] read_words;
    Button [] buttons;
    Label [] label;
    int ilosc_odgadnietyc_hasel=0;
    int ktore_klikniecie=1;
    int rand_nr;
    boolean czyPowtorzony=false;
    boolean czyPoczatek=true;
    boolean koniecGry=false;
    @FXML Label timeLabel;
    @FXML Label wybKatLabel;
    @FXML
    GridPane gridpane;
    Random generator=new Random();
    @FXML Label totalScoreLabel;

    EventHandler<ActionEvent> buttonHandler = event -> {
        Button source = (Button)event.getSource();
        source.setVisible(false);
        obslugaKlikniecia(source);
        event.consume();
    };


    public void addElementToGrid(String category) {
        totalScoreLabel.setText(Integer.toString(ControllerMenu.totalScore));
        wybKatLabel.setText(category);
        read_words = RWFile.ReadFile("src/db/Memory/"+category+".txt");
        wordsAll = new String[read_words.length / 2][2];
        int k = 0;
        for (int i = 0; i < read_words.length; i++) {
            wordsAll[k][0] = read_words[i++];
            wordsAll[k++][1] = read_words[i];
        }
        int rwlp = read_words.length / 2;

        int[] nr_wylosowanego_wyrazu = new int[ilosc_wyrazow / 2];
        for (int i = 0; i < ilosc_wyrazow / 2; i++) {

            rand_nr = 0;
            do {
                czyPowtorzony = false;
                rand_nr = generator.nextInt(rwlp);
                for (int l = 0; l < ilosc_wyrazow / 2; l++) {
                    if (nr_wylosowanego_wyrazu[l] == rand_nr) {
                        czyPowtorzony = true;
                        break;
                    }
                }
            } while (czyPowtorzony == true);
            nr_wylosowanego_wyrazu[i] = rand_nr;
            wordsToShow[i][0] = wordsAll[rand_nr][0];
            wordsToShow[i][1] = wordsAll[rand_nr][1];
            System.out.println(wordsToShow[i][0] + " odp: " + wordsToShow[i][1]);
        }
        k = 0;
        for (int i = 0; i < ilosc_wyrazow; i++) {
            words[i] = new Label();
            words[i++].setText(wordsToShow[k][0]);
            words[i] = new Label();
            words[i].setText(wordsToShow[k++][1]);
//            System.out.println(words[i - 1].getText());
//            System.out.println(words[i].getText());
        }

        int x = 0;
        int rand = 0;
        int tab_zaj[] = new int[16];
        label = new Label[16];
        buttons = new Button[16];
        for(int i=0;i<16;i++){
            label[i]=new Label();
            buttons[i]=new Button();
            buttons[i].setPrefHeight(120);
            buttons[i].setPrefWidth(120);
            buttons[i].setGraphic(new ImageView(imagePytajnik));
            buttons[i].setOnAction(buttonHandler);
        }
        for (int x1 = 0; x1 < 16; x1++) {
                tab_zaj[x1] = -1;

        }
        for (int nr_pytania = 0; nr_pytania < 8; nr_pytania++) {
            for (int pyt_odp = 0; pyt_odp < 2; pyt_odp++) {
                do {
                    czyPowtorzony=false;
                    rand = generator.nextInt(16);
                    if (tab_zaj[rand] == -1) {
                        label[rand].setText(wordsToShow[nr_pytania][pyt_odp]);
                        tab_zaj[rand]=1;
                        czyPowtorzony=false;
                    }else {
                        czyPowtorzony=true;
                    }
                } while (czyPowtorzony==true);
            }
        }
        k=0;
                for (int i = 0; i < ilosc_wyrazow / 4; i++) {
                    for (int j = 0; j < ilosc_wyrazow / 4; j++) {
                        label[k].setWrapText(true);
                        label[k].setMaxWidth(100);
                        label[k].setAlignment(Pos.CENTER);
                        GridPane.setConstraints(label[k], j,i);
                        GridPane.setConstraints(buttons[k],j,i);
                        GridPane.setHalignment(label[k], HPos.CENTER);
                        GridPane.setValignment(label[k], VPos.CENTER);
                        label[k].setVisible(false);
                        gridpane.getChildren().addAll(buttons[k],label[k++]);
                    }
                }

        showOneElementStart();
    }

    String wyraz1="X";
    String wyraz2="X";
    int nr_wyrazu_1=0;
    int nr_wyrazu_2=0;

    public void obslugaKlikniecia(Button source){
        czyPoczatek=false;
        source.setVisible(false);
        if(ktore_klikniecie==1){
            System.out.println("Wykrylem pierwsze klikniecie");
            for(int i=0;i<16;i++){
                if(source==buttons[i]){
                    buttons[i].setVisible(false);
                    label[i].setVisible(true);
                    wyraz1=label[i].getText();
                    nr_wyrazu_1=i;
                    break;
                }
            }
            ktore_klikniecie=2;
        }

        else if(ktore_klikniecie==2){
            System.out.println("Wykrylem drugie klikniecie");
            for(int i=0;i<16;i++){
                if(source==buttons[i]){
                    buttons[i].setVisible(false);
                    label[i].setVisible(true);
                    wyraz2=label[i].getText();
                    nr_wyrazu_2=i;
                    if(check(nr_wyrazu_1,nr_wyrazu_2,wyraz1,wyraz2)==false){
                            startTask();
                    }else{
                        //label[i].setBackground(new Background(new BackgroundFill(Color.GREEN, CornerRadii.EMPTY, Insets.EMPTY)));
                        ControllerMenu.totalScore++;
                        ilosc_odgadnietyc_hasel++;
                        totalScoreLabel.setText(Integer.toString(ControllerMenu.totalScore));
                        startOnOffLabel();
                        if(ilosc_odgadnietyc_hasel==8){
                            koniecGry=true;
                            memoryRun=false;
                            showAllert();
                        }
                    }
                }
            }
            ktore_klikniecie=1;
        }


    }


    private boolean check(int nr_wyrazu1,int nr_wyrazu2, String wyraz1, String wyraz2){
        boolean czy_poprawne=false;
        String drugi_wyraz="X";
loops:
        for(int i=0;i<8;i++){
            for(int j=0;j<2;j++){
                if(wordsToShow[i][j].equals(wyraz1)||wordsToShow[i][j].equals(wyraz2)){
                    if(j==0){
                        drugi_wyraz=wordsToShow[i][1];
                        break loops;
                    }
                    else{
                        drugi_wyraz=wordsToShow[i][0];
                        break loops;
                    }
                }
            }
        }

        if(drugi_wyraz.equals(wyraz1)||drugi_wyraz.equals(wyraz2)){
            czy_poprawne=true;
        }
        else czy_poprawne=false;

        for(int i=0; i<16;i++){
            buttons[i].setDisable(false);
        }

        return czy_poprawne;
    }

    public void backOneStep(int nr_wyrazu1, int nr_wyrazu2){
        if(czyPoczatek==false){
            for(int j=0;j<16;j++){
                buttons[j].setDisable(false);
            }
        }

        buttons[nr_wyrazu1].setVisible(true);
        buttons[nr_wyrazu2].setVisible(true);
        label[nr_wyrazu1].setVisible(false);
        label[nr_wyrazu2].setVisible(false);
    }

    @FXML
    Button buttonMenu;

    public void showMenu(){
        memoryRun=false;
        try {
            URL url = getClass().getClassLoader().getResource("sample/Controllers/FXMLTemplates/Menu.fxml");
            FXMLLoader loader = new FXMLLoader(url);
            Stage stage = (Stage) buttonMenu.getScene().getWindow();

            stage.setResizable(true);
            Scene scene = new Scene(loader.load());
            stage.setScene(scene);
            ControllerMenu cm = loader.getController();
            cm.setUserInfoLabel();

        }catch (IOException io){
            io.printStackTrace();
        }
    }

    public void startTimer(){
//        Runnable timer = new Runnable() {
//            @Override
//            public void run() {
//                showTime();
//            }
//        };
//        Thread backgroundThread = new Thread(timer);
//        backgroundThread.setDaemon(true);
//        backgroundThread.start();

//        new Thread(new Runnable() {
//            @Override public void run() {
//                while(koniecGry==false){
//                    Platform.runLater(new Runnable() {
//                        @Override public void run() {
//                            showTime();
//                        }
//                    });
//                }
//            }).start();
//    }
        Task task = new Task<Void>() {
            @Override public Void call() {
                while(koniecGry==false){
                    showTime();
                }
                return null;
            }
        };

        //ProgressBar bar = new ProgressBar();
        //bar.progressProperty().bind(task.progressProperty());
        new Thread(task).start();

    }


    private void showTime(){


        }




    public void startTask()
    {
        // Create a Runnable
        Runnable task = new Runnable()
        {
            public void run()
            {
                runTask();
            }
        };
        // Run the task in a background thread
        Thread backgroundThread = new Thread(task);
        // Terminate the running thread if the application exits
        backgroundThread.setDaemon(true);
        // Start the thread
        backgroundThread.start();
    }

    private void runTask(){
        if(czyPoczatek==false){
            for(int i=0;i<16;i++){
                buttons[i].setDisable(true);
            }
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            backOneStep(nr_wyrazu_1,nr_wyrazu_2);
        }
        else if(czyPoczatek==true){

            for(int i=0;i<16;i++){
                label[i].setVisible(true);
                buttons[i].setVisible(false);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                backOneStep(i,0);
            }
            for(int j=0;j<16;j++){
                buttons[j].setDisable(false);
            }
            //startTimer();
                Runnable r = new Timer(timeLabel);
                new Thread(r).start();

        }

    }

    public void startOnOffLabel()
    {
        // Create a Runnable
        Runnable task = new Runnable()
        {
            public void run()
            {
                runOnOffLabel();
            }
        };
        // Run the task in a background thread
        Thread backgroundThread = new Thread(task);
        // Terminate the running thread if the application exits
        backgroundThread.setDaemon(true);
        // Start the thread
        backgroundThread.start();
    }

    private void runOnOffLabel(){
        for(int i=0;i<16;i++){
            buttons[i].setDisable(true);
        }
        for(int i=0;i<3;i++){
            onoffLabel(false);
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            onoffLabel(true);
            try {
                Thread.sleep(700);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            onoffLabel(false);
        }
        for(int i=0;i<16;i++){
            buttons[i].setDisable(false);
        }
        if(ilosc_odgadnietyc_hasel==8){
            for(int il=0; il<16; il++) {
                label[il].setVisible(true);
            }
        }
    }

    private void onoffLabel(boolean show){
        label[nr_wyrazu_1].setVisible(show);
        label[nr_wyrazu_2].setVisible(show);
    }

    private void showAllert(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Good job!");
        alert.setHeaderText("VICTORY! You are very good player!");
        alert.setContentText("Your time "+Timer.timeToShowLabel);

        alert.showAndWait();
    }


    private void showOneElementStart(){
        czyPoczatek=true;
        startTask();
        for(int i=0;i<16;i++){
            buttons[i].setDisable(true);
            buttons[i].focusTraversableProperty().setValue(false);
        }
    }
}
