package sample.Controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import sample.Question;
import sample.RWFile;

import java.io.IOException;
import java.net.URL;
import java.util.Random;

public class ControllerCTABC {
    @FXML
    private Label q1;
    @FXML
    private Label q2;
    @FXML
    private Label q3;
    @FXML
    private RadioButton a1_1;
    @FXML
    private RadioButton a1_2;
    @FXML
    private RadioButton a1_3;
    @FXML
    private RadioButton a2_1;
    @FXML
    private RadioButton a2_2;
    @FXML
    private RadioButton a2_3;
    @FXML
    private RadioButton a3_1;
    @FXML
    private RadioButton a3_2;
    @FXML
    private RadioButton a3_3;
    @FXML
    private Label totalScoreLabel;
    @FXML
    private Button checkButton;
    @FXML Label wybranyCzasLabel;
    @FXML void initialize(){
        pytaniaLabel[0]=q1;
        pytaniaLabel[1]=q2;
        pytaniaLabel[2]=q3;
        odpowiedziRB[0][0]=a1_1;
        odpowiedziRB[0][1]=a1_2;
        odpowiedziRB[0][2]=a1_3;
        odpowiedziRB[1][0]=a2_1;
        odpowiedziRB[1][1]=a2_2;
        odpowiedziRB[1][2]=a2_3;
        odpowiedziRB[2][0]=a3_1;
        odpowiedziRB[2][1]=a3_2;
        odpowiedziRB[2][2]=a3_3;
        showAll();
    }
      String[] linie_wczytane= RWFile.ReadFile("src/db/ABC/"+ControllerMenu.category+".txt"); //sciezka do zmiany!!
      int ilosc_pytan;
      int[] wylosowane_pytania=new int[3];
      Label[] pytaniaLabel=new Label[3];
      RadioButton[][] odpowiedziRB=new RadioButton[3][3];
      Question[] tablica_pytan;
      String[][] tab_odp;




    Random generator=new Random();
    public ControllerCTABC(){

        for(int i=0;i<3;i++){
            wylosowane_pytania[i]=-1;
        }
        ilosc_pytan=linie_wczytane.length/2;
        tablica_pytan=new Question[ilosc_pytan];
        System.out.println("Ilosc pytan: "+ilosc_pytan);
        for(int i=0; i<3;i++){//3 bo losujemy 3 pytania
            int wylosowana_liczba=0;
            boolean czy_powtorzona=true;
            do {
                wylosowana_liczba = generator.nextInt(ilosc_pytan);
                for(int j=0;j<3;j++){
                    if(wylosowane_pytania[j]==wylosowana_liczba){
                        czy_powtorzona=true;
                        break;
                    }else czy_powtorzona=false;
                }
            }while(czy_powtorzona==true);
            wylosowane_pytania[i]=wylosowana_liczba;
            System.out.println(wylosowane_pytania[i]);
        }
        int j=0;
        tab_odp=new String[ilosc_pytan][];
        for(int i=0;i<ilosc_pytan;i++) {
            tablica_pytan[i]=new Question();
            tablica_pytan[i].setPytanie(linie_wczytane[j]);
            j++;
            tablica_pytan[i].setOdpowiedz(linie_wczytane[j]);
            j++;
            tab_odp[i]=tablica_pytan[i].getOdp();
//            for(int k=0;k<4;k++){
//                System.out.println(tab_odp[k]);
//            }
        }
    }
    public void showAll(){
        for(int i=0; i<3;i++){
            pytaniaLabel[i].setText(tablica_pytan[wylosowane_pytania[i]].getPytanie());
            for(int j=0;j<3;j++){
            odpowiedziRB[i][j].setText(tab_odp[wylosowane_pytania[i]][j]);
            }
            System.out.println(tablica_pytan[wylosowane_pytania[i]].getPoprawna_odp());
        }
        totalScoreLabel.setText(Integer.toString(ControllerMenu.totalScore));
        wybranyCzasLabel.setText(ControllerMenu.category);
    }

    public void showMenu(){
        try {
            URL url = getClass().getClassLoader().getResource("sample/Controllers/FXMLTemplates/Menu.fxml");
            FXMLLoader loader = new FXMLLoader(url);
            Stage stage = (Stage) q1.getScene().getWindow();

            stage.setResizable(true);
            Scene scene = new Scene(loader.load());
            stage.setScene(scene);
            ControllerMenu cm = loader.getController();
            cm.setUserInfoLabel();

        }catch (IOException io){
            io.printStackTrace();
        }
    }

    public void sprawdzOdp(){
        System.out.println("Sprawdzam odp...");
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                if(odpowiedziRB[i][j].isSelected()){
                    if(tablica_pytan[wylosowane_pytania[i]].getPoprawna_odp()-1==j){
                        System.out.println("Poprawna odp dla pyt: "+Integer.toString(wylosowane_pytania[i]+1));
                        //odpowiedziRB[i][j].setBackground(new Background(new BackgroundFill(Color.GREEN, CornerRadii.EMPTY, Insets.EMPTY)));
                        odpowiedziRB[i][j].setBorder(new Border(new BorderStroke(Color.GREEN, BorderStrokeStyle.SOLID, new CornerRadii(5), new BorderWidths(3))));
                        ControllerMenu.totalScore++;
                        checkButton.setVisible(false);
                    }else{
                        System.out.println("Bledna odp  dla pyt: "+Integer.toString(wylosowane_pytania[i]+1));
                        //odpowiedziRB[i][j].setBackground(new Background(new BackgroundFill(Color.RED, CornerRadii.EMPTY, Insets.EMPTY)));
                        odpowiedziRB[i][j].setBorder(new Border(new BorderStroke(Color.RED, BorderStrokeStyle.SOLID, new CornerRadii(5), new BorderWidths(3))));
                        //odpowiedziRB[i][tablica_pytan[wylosowane_pytania[i]].getPoprawna_odp()-1].setBackground(new Background(new BackgroundFill(Color.GREEN, CornerRadii.EMPTY, Insets.EMPTY)));
                        odpowiedziRB[i][tablica_pytan[wylosowane_pytania[i]].getPoprawna_odp()-1].setBorder(new Border(new BorderStroke(Color.GREEN, BorderStrokeStyle.DOTTED, new CornerRadii(5), new BorderWidths(3))));
                    }
                }
            }
        }
        totalScoreLabel.setText(Integer.toString(ControllerMenu.totalScore));
    }
}
