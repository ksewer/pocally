package sample.Controllers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import sample.RWFile;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MemoryAddWordsController {
    @FXML
    ChoiceBox<String> categories;
    @FXML
    ListView <String> listWords;
    @FXML
    Label numberOfWordsLabel;
    @FXML
    AnchorPane editPane;
    @FXML
    AnchorPane buttonsPane;
    @FXML
    AnchorPane newCategoryPane;

    String[] read_words;
    String wybrana_kategoria="Starter Pack";
    int iloscSlowek=0;
    String[][] doubleWords;
    String[] words_to_save;
    int choosed_position;
    List<String[]> words_array = new ArrayList<String[]>();
    String[] read_category;
    List<String> categories_array = new ArrayList<>();

    public MemoryAddWordsController(){
        read_category=RWFile.ReadFile("src/db/Categories/categoriesMemory.txt");
        categories_array.clear();
        for(int i=0; i<read_category.length;i++){
            categories_array.add(read_category[i]);
        }
        readWordsCategory();
    }

    private void readWordsCategory(){
        read_words = RWFile.ReadFile("src/db/Memory/"+wybrana_kategoria+".txt");
        if(read_words.length>0){
            iloscSlowek=read_words.length/2;
            doubleWords=new String[iloscSlowek][2];
            int k=0;
            for(int i=0;i<read_words.length;i++){
                doubleWords[k][0]=read_words[i++];
                //System.out.println(doubleWords[k][0]);
                doubleWords[k++][1]=read_words[i];
                //System.out.println(doubleWords[k-1][1]);
            }
        }
    }

    private void showWords(){
        listWords.getItems().clear();
        iloscSlowek=words_array.size();
        for(int i=0; i<iloscSlowek;i++){
            listWords.getItems().add(words_array.get(i)[0]+"-"+words_array.get(i)[1]);
        }
        numberOfWordsLabel.setText("NUMBER OF WORDS: "+iloscSlowek);
    }

    public void showElement(){
        categories.setStyle("-fx-font: 25px \"Impact\";");
        for(int i=0; i<read_category.length;i++){
            categories.getItems().add(read_category[i]);
        }
        categories.setValue(read_category[0]);

        categories.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number number2) {
                wybrana_kategoria=categories.getItems().get((Integer) number2);
                System.out.println(wybrana_kategoria);
                listWords.getItems().clear();
                readWordsCategory();
                words_array.clear();
                if(read_words.length>0){
                    convertTabToArray();
                    showWords();
                }
                numberOfWordsLabel.setText("NUMBER OF WORDS: "+words_array.size());
            }
        });
        readWordsCategory();
        convertTabToArray();
        showWords();
    }

    private void convertTabToArray(){
        for(int i=0; i<iloscSlowek;i++){
            words_array.add(doubleWords[i]);
        }
    }

    private String[] convertArrayToTab(){
        int ilosc_pojedynczych_slowek=words_array.size()*2;
        words_to_save=new String[ilosc_pojedynczych_slowek];
        int k=0;
        for(int i=0;i<words_array.size();i++){
            for(int j=0;j<2;j++){
                words_to_save[k++]=words_array.get(i)[j];
                System.out.println(words_to_save[k-1]);
            }
        }
        return words_to_save;
    }
    boolean ifAddNew = true;
    public void addOption(){
        buttonsPane.setVisible(false);
        editPane.setVisible(true);
        ifAddNew=true;
        polWordLabel.setText("");
        engWordLabel.setText("");
    }
    @FXML
    TextField engWordLabel;
    @FXML
    TextField polWordLabel;
    @FXML TextField nameNewCat;
    public void editOption(){
        choosed_position = listWords.getSelectionModel().getSelectedIndex();
        if(choosed_position==-1){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning Dialog");
            alert.setHeaderText("You have not made a choice!");
            alert.setContentText("Select the question to edit!");
            alert.showAndWait();
        }else {
            buttonsPane.setVisible(false);
            editPane.setVisible(true);
            ifAddNew = false;

            System.out.println(choosed_position);
            System.out.println(words_array.get(choosed_position)[0] + " " + words_array.get(choosed_position)[1]);
            engWordLabel.setText(words_array.get(choosed_position)[0]);
            polWordLabel.setText(words_array.get(choosed_position)[1]);
        }
    }

    public void confirmAddEdit(){
        String[] doubleWordTemporary=new String[2];
        doubleWordTemporary[0]=engWordLabel.getText();
        doubleWordTemporary[1]=polWordLabel.getText();
        engWordLabel.setText("");
        polWordLabel.setText("");
        if(doubleWordTemporary[0].equals("")||doubleWordTemporary[1].equals("")){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning Dialog");
            alert.setHeaderText("Text Field is empty!");
            alert.setContentText("I can't add/modify empty word!");

            alert.showAndWait();
        }else{
        if(ifAddNew==true){
            words_array.add(doubleWordTemporary);
            showWords();
        }else{
            words_array.set(choosed_position,doubleWordTemporary);
            showWords();
        }
        backToOption();
        }
    }

    public void backToOption(){
        //alertSaveQuestion();
        numberOfWordsLabel.setVisible(true);
        buttonsPane.setVisible(true);
        editPane.setVisible(false);
        newCategoryPane.setVisible(false);
        showWords();
    }

    public void saveOption(){
        convertArrayToTab();
        RWFile.saveDataBase(convertArrayToTab(),"src/db/Memory/"+wybrana_kategoria+".txt");
        String[] categoriesToSave=new String[categories_array.size()];
        for(int i=0;i<categories_array.size();i++) {
            categoriesToSave[i]=categories_array.get(i);
            System.out.println("kategoria "+i+" "+categoriesToSave[i]);
        }
        RWFile.saveDataBase(categoriesToSave,"src/db/Categories/categoriesMemory.txt");
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        alert.setContentText("I have a great message for you! You saved the database correctly.");

        alert.showAndWait();
    }

    public void deleteWord(){
        choosed_position=listWords.getSelectionModel().getSelectedIndex();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText("Are you sure that you want delete this element:");
        alert.setContentText(words_array.get(choosed_position)[0]+" - "+words_array.get(choosed_position)[1]);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            words_array.remove(choosed_position);
        }
        showWords();
    }

    public void deleteCategory(){
        choosed_position=listWords.getSelectionModel().getSelectedIndex();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText("Are you sure that you want delete this element:");
        alert.setContentText(categories_array.get(choosed_position));

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            RWFile.deleteFile("src/db/Memory/"+categories_array.get(choosed_position)+".txt");
            categories_array.remove(choosed_position);
            showCategories();
        }
    }

    public void addCategoryOption(){
        read_category=RWFile.ReadFile("src/db/Categories/categoriesMemory.txt");
        buttonsPane.setVisible(false);
        newCategoryPane.setVisible(true);
        listWords.getItems().clear();
        nameNewCat.setText("");
        numberOfWordsLabel.setVisible(false);

        for(int i=0;i<read_category.length;i++){
            System.out.println(read_category[i]);
            listWords.getItems().add(read_category[i]);
            //categories_array.add(read_category[i]);
        }
    }

    public void showMenu(){
        alertSaveQuestion();
        try {
            URL url = getClass().getClassLoader().getResource("sample/Controllers/FXMLTemplates/Menu.fxml");
            FXMLLoader loader = new FXMLLoader(url);
            Stage stage = (Stage) polWordLabel.getScene().getWindow();

            stage.setResizable(true);
            Scene scene = new Scene(loader.load());
            stage.setScene(scene);
            ControllerMenu cm = loader.getController();
            cm.setUserInfoLabel();
        }catch (IOException io){
            io.printStackTrace();
        }
    }

    public void addCategory(){
        boolean ifRepeat=false;
        String name=nameNewCat.getText();
        nameNewCat.setText("");
        if(name.equals("")){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning Dialog");
            alert.setHeaderText("Text Field is empty!");
            alert.setContentText("I can't add/modify empty category!");

            alert.showAndWait();
        }else{
            for(int i=0; i<categories_array.size();i++){
                if(categories_array.get(i).equals(name)){
                    ifRepeat=true;
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Warning Dialog");
                    alert.setHeaderText("The category "+name+" already exist");
                    alert.setContentText("I can't add this category!");

                    alert.showAndWait();
                    break;
                }
            }
            if(ifRepeat==false){
                categories_array.add(name);
                showCategories();
                RWFile.createNewFile("src/db/Memory/"+name+".txt");
                }
            }
        }

        private void showCategories(){
            listWords.getItems().clear();
            listWords.getItems().add(categories_array.get(0));
            //System.out.println(categories_array.get(0));
            int il_kat=categories.getItems().size();
            //System.out.println(il_kat);
            for (int i=1;i<categories_array.size();i++){
                if(i<il_kat){
                    categories.getItems().set(i,categories_array.get(i));
                }else{
                    categories.getItems().add(categories_array.get(i));
                }
                listWords.getItems().add(categories_array.get(i));
            }
            categories.setValue(categories_array.get(0));
        }

        private void alertSaveQuestion(){
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirmation Dialog");
            alert.setHeaderText("Do you want to save?");
            alert.setContentText("Choose your option.");

            ButtonType buttonTypeYES = new ButtonType("YES");
            ButtonType buttonTypeNO = new ButtonType("NO");
            //ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);

            alert.getButtonTypes().setAll(buttonTypeYES, buttonTypeNO);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == buttonTypeYES){
                saveOption();
            }
        }
}

