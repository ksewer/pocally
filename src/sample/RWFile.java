package sample;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static sample.Controllers.LoginController.users;

public class RWFile {

    public static String[] ReadFile(String path){
        List<String> linijki_wczytane=new ArrayList<String>();
        Scanner read;
        String[] linie;
        String error[]=new String[1];
        error[0]="Nieudany oddczyt z pliku";


        {
            try {
                read = new Scanner(new File(path));
                while (read.hasNext()) {
                    linijki_wczytane.add(read.nextLine());
                }
                linie=new String[linijki_wczytane.size()];
                for(int i=0; i<linijki_wczytane.size();i++){
                    linie[i]=linijki_wczytane.get(i);
                    //System.out.println(linie[i]);
                }
                read.close();
                return linie;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return error;
            }
        }

    }

    public static void saveNewAccount(User[] users){
        PrintWriter zapis;
        try {
            zapis = new PrintWriter("src/db/users.txt");
            for(int i=0;i<users.length;i++){
                zapis.println("next");
                zapis.println(users[i].getLogin());
                zapis.println(users[i].getPassword());
                zapis.println(users[i].score);
                zapis.println(users[i].getEmail());
            }
            zapis.close();
            System.out.println("Zapis pozycji zostal wykonany pomyślnie.");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void saveDataBase(String[] words,String path){
        PrintWriter zapis;
        try {
            zapis = new PrintWriter(path);
            for(int i=0;i<words.length;i++){
                zapis.println(words[i]);
            }
            zapis.close();
            System.out.println("Zapis pozycji zostal wykonany pomyślnie.");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void createNewFile(String path){
        try {

            File file = new File(path);

            if (file.createNewFile()){
                System.out.println("Plik zostal stworzony!");
            }else{
                System.out.println("Plik juz istnieje");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void deleteFile(String path){
        try{

            File file = new File(path);

            if(file.delete()){
                System.out.println(file.getName() + " zostal skasowany!");
            }else{
                System.out.println("Operacja kasowania sie nie powiodla.");
            }

        }catch(Exception e){

            e.printStackTrace();

        }
    }

}
