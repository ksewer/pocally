package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.Controllers.LoginController;

public class Main extends Application {
    public static Parent root;
    public static Stage MainStage;

    @Override
    public void start(Stage primaryStage) throws Exception{
        root = FXMLLoader.load(getClass().getResource("Controllers/FXMLTemplates/Login.fxml"));
        primaryStage.setTitle("Project English");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
        MainStage=primaryStage;
    }


    public static void main(String[] args) {
        launch(args);
    }
}
