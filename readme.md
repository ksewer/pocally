# PoCaLLy

> PoCaLLy-Play, Create database and Learn Language with this app.

##Table of Contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [To-do list](#to-do list)
* [Status](#status)
* [Contact](#contact)

## General info
The project was created to pass the English language course in college. In this app are a few options for learning: Hangman, Mixed-words, ABC and Memory-game.

* Hangman: The user using on-screen keyboard tries to guess the word drawn from the database. The player may make 7 mistakes. 8 mistake = defeat.

* Mixed-words: The user get mixed words and must arrange them in correct order.

* ABC: The user reveives a sentence with the missing element and must choose the correct answer from the 3 given.

* Memory-game: The app draws 8 pairs of words. At the begining each word is visible for 0.3 sec. The user must match the words in pairs.

The project includes a package of words - Starter Pack. Each user can create his own database and use it in each of the above categories.

## Technologies
* Java 8
* intelliJ IDEA
* Scene Builder

## Setup
Download the catalog 'out' from this project. 
* Windows: Open file 'Run PoCaLLy.bat' (double click). 
* Linux: Open file 'PoCaLLy.jar' (double click).

The app will be launched*.

* JRE (Java Runtime Environment) is required to launch this application.

## To-do list
* Transfer of the words database and user database to the server.
* The user will be able to log in using the GOOGLE account.

## Status
Project is in progress.

## Contact
Created by Kacper Seweryn(kacper.seweryn18@gmail.com) - fell free to contact me!
